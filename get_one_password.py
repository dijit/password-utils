#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from sys import argv, stdout, stderr
from getpass import getpass
import argparse
from os import path
import blimey

PARSER = argparse.ArgumentParser()
PARSER.add_argument("-g", "--force_gui",
                    help="Force QT GUI (requires PyQT4)",
                    action="store_true")
PARSER.add_argument("-u", "--pass_uuid",
                    help="password UUID",
                    type=str, default="E78A6F95EF754AF8913D54503D8A1C41")
PARSER.add_argument("-p", "--path",
                    help="Path to agilekeychain",
                    type=str, default="~/1Password.agilekeychain")
PARSER.add_argument("-o", "--stdout",
                    help="Output password to STDOUT, useful for commandline \
                            applications called with $({} -o)".format(argv[0]),
                    action="store_true")
PARSER.add_argument("-v", "--verbose",
                    help="Verbosity, for debugging",
                    action="store_true")
ARGS = PARSER.parse_args()
del PARSER, argparse

if not stdout.isatty() or ARGS.force_gui:
    QT_ENABLED = True
else:
    QT_ENABLED = False

AK_PATH = path.expanduser(ARGS.path)

if QT_ENABLED:
    from PyQt4 import QtGui

    class Unlock(QtGui.QDialog):
        def __init__(self, parent=None):
            super(Unlock, self).__init__(parent)
            self.password = ''
            self.text_pass = QtGui.QLineEdit(self)
            self.text_pass.setEchoMode(QtGui.QLineEdit.Password)
            self.button_unlock = QtGui.QPushButton('Unlock', self)
            self.button_unlock.clicked.connect(self.handle_unlock)
            layout = QtGui.QVBoxLayout(self)
            layout.addWidget(self.text_pass)
            layout.addWidget(self.button_unlock)

        def handle_unlock(self):
            self.password = self.text_pass.text()
            AGILEKEYCHAIN.unlock(self.password)
            self.done(0)

if __name__ == '__main__':
    AGILEKEYCHAIN = blimey.AgileKeychain(AK_PATH)

    while True:
        if QT_ENABLED:
            if ARGS.verbose:
                print("GUI Mode Enabled", file=stderr)
            APP = QtGui.QApplication(argv)
            Unlock().exec_()
        else:
            if ARGS.verbose:
                print("CLI Mode Enabled", file=stderr)
            AGILEKEYCHAIN.unlock(getpass())
        break

    ITEM = AGILEKEYCHAIN[ARGS.pass_uuid]
    # ['fields'][0]  = user
    # ['fields'][1]  = password
    # ['fields'][2:] = older  passwords.
    OUTPUT = ITEM.get('encrypted')['fields'][1]['value']
    if ARGS.stdout:
        print(OUTPUT)
    else:
        from time import sleep
        import clip
        OLDPASTE = clip.paste()
        clip.copy(OUTPUT)
        for i in reversed(range(10)):
            sleep(1)
            print("You have a password in your clipboard for \
                    {} seconds".format(i), file=stderr, end='\r')
        clip.copy(OLDPASTE)
