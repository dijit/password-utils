Blimey!
=======

I write some python scripts to interact with agilekeychain on linux.

These are probably *very* awful, please beware.

All scripts will assume you're using the command line, and may fall back to using PyQT4 if there's no interactive TTY.
*Why?*: Because PySide SUCKS! it doesn't support python3.5 and compiling it from source was literally impossible.

`pip install -r requirements.txt` will pull in blimey and getpass, but PyQT4 must be installed manually [here](http://pyqt.sourceforge.net/Docs/PyQt4/installation.html)

(on most platforms there is a package included by the OS maintainers, `python3-PyQt4` on Fedora for example)

get_one_password.py
-------------------

meant to be used as part of shell scripts which need a password on the command line:

```bash
xfreerdp /u:dijit /p:$(~/bin/get_one_password.py --stdout --path \
  ~/1Password.agilekeychain --uuid E78A6F95EF754AF8913D54503D8A1C41 -g) $host
```

Basically asks for your 1password unlock password and returns a password to 
  stdout.
the `-g` option will force a pyqt4 GUI to prompt you for a password, if, like
  me you enjoy running hotkeys for your programs that are long running.
You will almost certainly want to change the `agilekeychain_path` 
  and `password_uuid`
