#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The idea here is to list all the UUIDs in the agilekeychain, with some identifying information..
We can do this _without_ unlocking the keychain, but, I'm too fucking lazy.

nvm, did it without unlocking the keychain. Fuck you.
"""
import json
import os
import glob
from collections import defaultdict

class Item(defaultdict):
    def __init__(self, data):
        super().__init__(None, data)

    def __missing__(self, key):
        return None

    def __repr__(self):
        return '{0}({1})'.format(self.__class__.__name__, repr(dict(self)))

class ItemManager:
    def __init__(self, path):
        self._base_path = path

    def get_by_id(self, item_id):
        item_path = os.path.join(self._base_path, "data", "default", item_id + ".1password")

        try:
            with open(item_path, 'r') as file:
                data = json.load(file)
        except FileNotFoundError:
            pass

        item = Item(data)

        if self._is_not_password(item):
            return None

        return item

    def get_all_items(self):
        item_paths = glob.glob(os.path.join(self._base_path, "data", "default", "*.1password"))

        items = []
        for item_path in item_paths:
            basename = os.path.basename(item_path)
            item_id, _ = os.path.splitext(basename)
            item = self.get_by_id(item_id)
            if item is not None:
                items.append(item)
            else:
                print("Skipping")
        return items

    def _is_not_password(self, item):
        """
        There are standard types in an agilekeychain wallet that are not for passwords.
        We ignore them because SSN's should not be passwords.
        :param item:
        :return: True if dictionary should not have a password in it.
        """
        if item['typeName'] in ['wallet.computer.License','wallet.financial.CreditCard',
                                'wallet.government.SsnUS','system.folder.Regular',
                                'wallet.government.Passport','wallet.financial.BankAccountUS',
                                'securenotes.SecureNote']:
            return True
        else:
            return False

if __name__ == '__main__':
    # TODO: Allow this to be configured.
    path = "/home/dijit/1Password.agilekeychain"
    fucked_up_keychain = ItemManager(path)
    #print(fucked_up_keychain.get_all_items())
    for item in fucked_up_keychain.get_all_items():
        print("{}\t : {} :: {}".format(item['uuid'], item['title'], item['location']))