"""
Clip

A clipboard module for Python. (only handles plain text)
BSD License

Usage:
  import clip
  clip.copy('The text to be copied to the clipboard.')
  spam = clip.paste()

  if not clip.copy:
    print("Copy functionality unavailable!")

On Mac, the module uses pbcopy and pbpaste, which should come with the os.
On Linux, install xclip or xsel via package manager. For example, in Debian:
sudo apt-get install xclip

"""
__version__ = '0.0.1b'

import platform
import os
import subprocess
from .clipboards import (init_osx_clipboard,
                         init_xclip_clipboard, init_xsel_clipboard,
                         init_klipper_clipboard, init_no_clipboard)

def _executable_exists(name):
    return subprocess.call(["which", name],
                           stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0


def determine_clipboard():
    # Determine the OS/platform and set
    # the copy() and paste() functions accordingly.
    if os.name == 'mac' or platform.system() == 'Darwin':
        return init_osx_clipboard()
    if _executable_exists("xclip"):
        return init_xclip_clipboard()
    if _executable_exists("xsel"):
        return init_xsel_clipboard()
    if _executable_exists("klipper") and _executable_exists("qdbus"):
        return init_klipper_clipboard()

    return init_no_clipboard()


def set_clipboard(clipboard):
    global copy, paste

    clipboard_types = {'osx': init_osx_clipboard,
                       'xclip': init_xclip_clipboard,
                       'xsel': init_xsel_clipboard,
                       'klipper': init_klipper_clipboard,
                       'no': init_no_clipboard}

    copy, paste = clipboard_types[clipboard]()


copy, paste = determine_clipboard()

__all__ = ["copy", "paste"]
